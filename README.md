[![build status](https://gitlab.com/SubChord/sokoban-kotlinfx/badges/master/pipeline.svg)](https://gitlab.com/SubChord/sokoban-kotlinfx/pipelines)

# Sokoban
Sokoban build with Kotlin and TornadoFX (JavaFX)

## Build
`./gradlew build`

