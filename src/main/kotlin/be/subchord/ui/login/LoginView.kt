package be.subchord.ui.login

import javafx.beans.property.SimpleStringProperty
import tornadofx.*


class LoginView : View("Login") {
    private val loginController: LoginController by inject()

    private val loginModel = ViewModel()
    private val username = loginModel.bind { SimpleStringProperty() }
    private val password = loginModel.bind { SimpleStringProperty() }

    override val root = pane {
        form {
            fieldset {
                field("Username") {
                    textfield(username).required()
                }
                field("Password") {
                    textfield(password).required()
                }
                button {
                    text = "login"
                    action {
                        loginModel.commit()
                        if (loginModel.isValid) {
                            loginController.login(username.value, password.value)
                        }
                    }
                }
            }
        }
    }
}
