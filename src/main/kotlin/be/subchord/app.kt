package be.subchord

import be.subchord.ui.login.LoginView
import tornadofx.App
import tornadofx.launch

class MyApp() : App(LoginView::class) {}

fun main(args: Array<String>) {
    launch<MyApp>(args)
}

